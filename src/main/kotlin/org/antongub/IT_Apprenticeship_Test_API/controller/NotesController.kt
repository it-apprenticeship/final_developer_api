package org.antongub.IT_Apprenticeship_Test_API.controller

import org.antongub.IT_Apprenticeship_Test_API.model.Note
import org.antongub.IT_Apprenticeship_Test_API.service.NotesService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/notes")
class NotesController(
        private val notesService: NotesService
) {
    @GetMapping
    fun index(): List<Note> {
        return notesService.getShuffledData()
    }

    @GetMapping("/random")
    fun getRandom(): Note {
        return notesService.getRandomData()
    }
}
