package org.antongub.IT_Apprenticeship_Test_API.controller

import org.antongub.IT_Apprenticeship_Test_API.model.Question
import org.antongub.IT_Apprenticeship_Test_API.service.QuestionService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/questions")
class QuestionController(
        private val questionService: QuestionService
) {
    @GetMapping
    fun index(): List<Question> {
        return questionService.getShuffledData()
    }
}
