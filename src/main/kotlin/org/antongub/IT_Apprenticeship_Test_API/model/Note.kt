package org.antongub.IT_Apprenticeship_Test_API.model

data class Note(
        val id: Int,
        val category: String,
        val subCategory: String?,
        val text: String
)
