package org.antongub.IT_Apprenticeship_Test_API.model

data class Question(
        val id: Int,
        val category: String,
        val type: String,
        val text: String,
        val answers: AnswerGroup
)
