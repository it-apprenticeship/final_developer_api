package org.antongub.IT_Apprenticeship_Test_API.model

data class AnswerGroup(
        val type: String,
        val data: List<Answer>
)
