package org.antongub.IT_Apprenticeship_Test_API.model

data class Answer(
        val id: Int,
        val text: String,
        val correct: Boolean
)
