package org.antongub.IT_Apprenticeship_Test_API.service

import com.google.gson.Gson
import org.springframework.stereotype.Service
import java.io.FileNotFoundException
import java.io.Reader
import java.lang.Exception
import java.lang.reflect.Type
import java.nio.file.Files
import java.nio.file.Paths

@Service
class JsonService {
    fun <T> readFile(filePath: String, type: Type): List<T> {
        val gson = Gson()
        val resource = JsonService::class.java.getResource(filePath)
                ?: throw FileNotFoundException("File at $filePath not Found")
        val reader: Reader = Files.newBufferedReader(Paths.get(resource.toURI()))
        val data: List<T> = gson.fromJson(reader, type)
                ?: throw Exception("Could not deserialize the $filePath file")
        reader.close()

        return data
    }
}
