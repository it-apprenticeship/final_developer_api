package org.antongub.IT_Apprenticeship_Test_API.service

import com.google.gson.reflect.TypeToken
import org.antongub.IT_Apprenticeship_Test_API.model.Note
import org.springframework.stereotype.Service
import java.lang.reflect.Type

@Service
class NotesService(jsonService: JsonService) : FileService<Note>(jsonService) {
    override val filePath = "/data/notes.json"
    override val type: Type = object : TypeToken<List<Note?>?>() {}.type
}
