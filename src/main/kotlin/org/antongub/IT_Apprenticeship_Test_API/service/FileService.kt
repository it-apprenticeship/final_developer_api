package org.antongub.IT_Apprenticeship_Test_API.service

import java.lang.reflect.Type

abstract class FileService<T>(
        private val jsonService: JsonService
) {
    abstract val filePath: String
    abstract val type: Type

    fun getData(): List<T> = jsonService.readFile(filePath, type)
    fun getShuffledData(): List<T> = getData().shuffled()
    fun getRandomData(): T = getData().shuffled().first()
}
