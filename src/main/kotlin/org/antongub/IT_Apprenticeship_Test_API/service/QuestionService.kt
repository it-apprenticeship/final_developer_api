package org.antongub.IT_Apprenticeship_Test_API.service

import com.google.gson.reflect.TypeToken
import org.antongub.IT_Apprenticeship_Test_API.model.Question
import org.springframework.stereotype.Service
import java.lang.reflect.Type

@Service
class QuestionService(jsonService: JsonService) : FileService<Question>(jsonService) {
    override val filePath = "/data/questions.json"
    override val type: Type = object : TypeToken<List<Question?>?>() {}.type
}
