# IT Apprenticeship Test API

## Description
This API stores data for some possible questions and answers that could appear in a final test of a german "Ausbildung"/apprenticeship.

## TriviaIT_Apprenticeship_Quiz_API
In Germany there exists the concept of a "Duale Ausbildung".
It is a form of apprenticeship where a student attends school and works simultaneously. 
The student gains both theoretical and practical knowledge at the same time.
At the end of this "Ausbildung", which normaly lasts 3 years, the students have to absolve a test.
